# Python数据可视化
## 4 pyecharts库应用

​		[Echarts](https://github.com/apache/echarts) 是百度开源的一个数据可视化 JS 库，是Enterprise Charts的缩写，商业级数据图表，一个纯Javascript的图表库，提供直观，生动，可交互，可高度个性化定制的数据可视化图表

##### 数据集说明

​		本实验所采用的数据集为某医院2016年度部分月份的药品销售数据，共含有购药时间（包含具体日期及所处周内位置，后期数据处理将具体日期与星期拆分）、社保卡号、商品编码、商品名称、销售数量、应收金额、实收金额7个字段，总计6578条记录。

### 4.1 药品每周销售数量趋势
![药品每周销售数量趋势](https://gitee.com/chenzi2401/data-visualization/raw/master/image/%E8%8D%AF%E5%93%81%E9%94%80%E5%94%AE%E6%95%B0%E9%87%8F%E6%AF%8F%E5%91%A8%E8%B6%8B%E5%8A%BF.png)
### 4.2 每天药品销售数量变化趋势
![每天药品销售数量变化趋势](https://gitee.com/chenzi2401/data-visualization/raw/master/image/%E8%8D%AF%E5%93%81%E9%94%80%E5%94%AE%E6%95%B0%E9%87%8F%E6%AF%8F%E5%A4%A9%E8%B6%8B%E5%8A%BF.png)
### 4.3 不同药品的销售量分布

使用pyecharts完成不同药品的的销售量分布图，展示前十项。

### 4.4 不同药品的销售量与销售额分布

### 4.5 药品订单量的月度分布
使用pyecharts完成药品订单量的月度分布图（玫瑰图）
![药品订单量的月度分布](https://gitee.com/chenzi2401/data-visualization/raw/master/image/%E8%8D%AF%E5%93%81%E8%AE%A2%E5%8D%95%E9%87%8F%E7%9A%84%E6%9C%88%E5%BA%A6%E5%88%86%E5%B8%83.png)


