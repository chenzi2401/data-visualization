import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
plt.rcParams['font.sans-serif'] = ['Microsoft YaHei']  # 解决中文乱码问题
plt.rcParams['axes.unicode_minus'] = False  # 解决负号无法显示的问题

# 步骤一：数据加载与预处理
# 读取数据
data = pd.read_excel('dataset.xlsx')

# 选择所需的列
selected_columns = ['division', 'price', 'area', 'school', 'hospital', 'balcony', 'bed', 'closet', 'sofa', 'TV', 'fridge', 'washing_machine', 'AC', 'heater', 'wifi', 'gas']
# 计算配备设施总数
data['facilities total'] = data[['balcony', 'bed', 'closet', 'sofa', 'TV', 'fridge', 'washing_machine', 'AC', 'heater', 'wifi', 'gas']].sum(axis=1)
# 选择所需的列
df = data[['division', 'price', 'area', 'school', 'hospital', 'facilities total']]


# 处理缺失值（如果有的话）
df = df.dropna()
# 显示处理后的数据
print(df.head())


# 步骤二：按行政区分组并计算均值
# 按行政区分组，计算均值
grouped_data = df.groupby('division').mean()
# 显示分组后的数据
print(grouped_data)

# 步骤三：归一化处理
# 归一化处理
normalized_data = pd.DataFrame()
columns = df.columns.tolist()
for column in columns[1:]:
    normalized_data[column] = grouped_data[column] / grouped_data[column].max()

# 显示归一化后的数据
print(normalized_data)



# 步骤四：绘制极坐标图
# 绘制极坐标图
plt.figure(figsize=(10, 10))
# 设置角度
label = ['price', 'area', 'school', 'hospital', 'facilities total']
label_angles = [i / float(len(label)) * 2 * np.pi for i in range(len(label))]
angles = [i / float(len(columns) - 1) * 2 * np.pi for i in range(len(columns))]
for i, division in enumerate(normalized_data.index):
    values = normalized_data.loc[division].values.flatten().tolist()
    values += values[:1]
    ax = plt.subplot(111, polar=True)
    ax.plot(angles, values, label=division)
    # ax.fill(angles, values, label=division)
    ax.set_yticklabels([])
    ax.set_xticks(label_angles)
    ax.set_xticklabels(label)

plt.legend(loc='upper right')
plt.show()
