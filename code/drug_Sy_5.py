import pandas as pd
import pyecharts.options as opts
from pyecharts.charts import Pie
from pyecharts.commons.utils import JsCode

# 读取数据
df = pd.read_excel('../Data/医院药品销售数据.xlsx', converters={'社保卡号':str, '商品编码':str, '销售数量':int})
df2 = df.copy()
df2 = df2.dropna(subset=['购药时间'])
df2['社保卡号'].fillna('0000', inplace=True)
df2['销售数量'] = df2['销售数量'].astype(int)

df2['销售数量'] = df2['销售数量'].abs()
df2['应收金额'] = df2['应收金额'].abs()
df2['实收金额'] = df2['实收金额'].abs()

# 列拆分（购药时间列拆分为两列）
df3 = df2.copy()
df3[['购药日期', '星期']] = df3['购药时间'].str.split(' ', expand=True, n=2)
df3 = df3[['购药日期','社保卡号','商品编码', '商品名称', '销售数量', '应收金额', '实收金额']]
print(df3.head(5))

# 添加月数列
# Convert the '购药日期' column to datetime format
df3['购药日期'] = pd.to_datetime(df3['购药日期'])

# Extract month from the '购药日期' column
df3['月份'] = df3['购药日期'].dt.to_period('M')

# Group by month and sum the sales quantity
monthly_sales = df3.groupby('月份')['销售数量'].sum().reset_index()

# Display the result
print(monthly_sales)

# Define a more intricate blue color gradient
intricate_blue_gradient_color = JsCode(
    """new echarts.graphic.LinearGradient(0, 0, 0, 1, [
        {offset: 0, color: 'rgba(70,130,180,0.9)'},
        {offset: 0.3, color: 'rgba(70,130,180,0.7)'},
        {offset: 0.6, color: 'rgba(70,130,180,0.5)'},
        {offset: 1, color: 'rgba(70,130,180,0.3)'}
    ])"""
)

# Create a Pie chart
rose_chart = (
    Pie()
    .add(
        series_name="月销售量",
        data_pair=list(zip(monthly_sales['月份'].astype(str), monthly_sales['销售数量'])),
        radius=["30%", "75%"],  # Set the radius for the rose chart
        center=["30%", "50%"],  # Set the center of the chart
        rosetype="area",  # Set the chart type to rose
        label_opts=opts.LabelOpts(
            is_show=True,
            position="outside",  # Display labels outside the sectors
            formatter="{b}\n{c}"
        ),
        itemstyle_opts=opts.ItemStyleOpts(
            color=intricate_blue_gradient_color
        ),
    )
    .set_global_opts(
        title_opts=opts.TitleOpts(title="药品订单量的月度分布", subtitle="数据来源：医院药品销售"),
        legend_opts=opts.LegendOpts(is_show=False),  # Hide legend for better visualization
        toolbox_opts=opts.ToolboxOpts(),  # Enable toolbox for additional functionalities
    )
    .render("../html/药品订单量的月度分布.html")
)
