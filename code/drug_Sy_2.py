import pandas as pd
import pyecharts.options as opts
from pyecharts.charts import Scatter

# 读取数据
df = pd.read_excel('../Data/医院药品销售数据.xlsx', converters={'社保卡号':str, '商品编码':str, '销售数量':int})
# print(df.info())
# print(df.head(10))
df2 = df.copy()
df2 = df2.dropna(subset=['购药时间'])
df2['社保卡号'].fillna('0000', inplace=True)
df2['销售数量'] = df2['销售数量'].astype(int)
# print(df2[['销售数量','应收金额','实收金额']].describe())
# print(df2.loc[(df2['销售数量'] < 0)])
df2['销售数量'] = df2['销售数量'].abs()
df2['应收金额'] = df2['应收金额'].abs()
df2['实收金额'] = df2['实收金额'].abs()

# print(df2.loc[(df2['销售数量'] < 0) | (df2['应收金额'] < 0) | (df2['实收金额'] < 0)].sum())

# 列拆分（购药时间列拆分为两列）
df3 = df2.copy()
df3[['购药日期', '星期']] = df3['购药时间'].str.split(' ', expand=True, n=2)

df3 = df3[['购药日期','社保卡号','商品编码', '商品名称', '销售数量', '应收金额', '实收金额']]

# 忽略“星期”列输出
# print(df3.groupby('购药日期')[['销售数量', '应收金额', '实收金额']].sum())

# 添加周数列
df3['购药日期'] = pd.to_datetime(df3['购药日期'])
# df3['week'] = df3['购药日期'].dt.isocalendar().week
df3['day'] = df3['购药日期'] - pd.to_datetime('2016-01-01')
# df3 = df3.sort_values('week')
# 提取数值

df3['day'] = df3['day'].astype(str)
# print(df3.head(20))
# 将字符串转换为数字：
df3['day'] = df3['day'].str.replace(" days", "").astype(int)
print(df3.head(20))
# print(df3.info())
# 取出销售数量数据
sales_data = df3['销售数量'].tolist()
# 取出周数数据
# week_data = df3['week'].tolist()
# 取出天数数据
day_data = df3['day'].tolist()
# 修改周数
# week_data = [1 if i == 53 else i + 1 for i in week_data]
day_data = [i + 1 for i in day_data]

# 绘制散点图
scatter1 = (
    Scatter()
    .add_xaxis(day_data)
    .add_yaxis(
        # 系列名称
        series_name='销售量',
        # 系列数据
        y_axis=sales_data,
        label_opts=opts.LabelOpts(is_show=False)  # 不显示标签
    )
    # 全局配置项
    .set_global_opts(
        title_opts=opts.TitleOpts(title='药品销售数量每天趋势'),
        # x轴配置
        xaxis_opts=opts.AxisOpts(
            name='天数',
            type_="value",
        ),
        # y轴配置
        yaxis_opts=opts.AxisOpts(
            name='销售量',
            type_="value",
        ),
        # 颜色映射
        visualmap_opts=opts.VisualMapOpts(max_=50),
    )
)
scatter1.render('../html/药品销售数量每天趋势.html')
